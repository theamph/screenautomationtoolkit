﻿using Autofac;
using Moq;
using ScreenToolkit.Windows;
using System;
using System.Collections.Generic;

namespace ScreenToolkit.Tests
{
    public class AutoMockingFixture
    {
        protected IContainer Container { get; set; }
        protected bool ResolveRealDependencies { get; set; }
        private IDictionary<Type, Mock> _configuredMocks { get; set; }

        public AutoMockingFixture()
        {
            _configuredMocks = new Dictionary<Type, Mock>();
            BuildContainer();
        }

        /// <summary>
        /// Builds or rebuilds the Autofac container
        /// </summary>
        /// <param name="sut">The type of the system under test</param>
        protected void BuildContainer(Type sut = null)
        {
            var builder = new ContainerBuilder();
            if (ResolveRealDependencies)
                builder.RegisterModule(new WindowsModule());
            else
            {
                builder.RegisterModule(new MockModule());
                foreach (var mock in _configuredMocks)
                {
                    builder
                        .RegisterInstance(mock.Value.Object)
                        .As(mock.Key);
                }
            }
            if (sut != null)
                builder.RegisterType(sut);

            Container = builder.Build();
        }

        /// <summary>
        /// Configures the fixture to resolve real dependencies over mocks
        /// </summary>
        /// <typeparam name="T">The type of the fixture</typeparam>
        /// <returns></returns>
        public T WithRealDependencies<T>() where T : AutoMockingFixture
        {
            ResolveRealDependencies = true;
            BuildContainer();
            return this as T;
        }

        /// <summary>
        /// Creates an instance of the system under test
        /// </summary>
        /// <typeparam name="T">The type of the system under test</typeparam>
        /// <returns></returns>
        public T CreateSut<T>() where T : class
        {
            BuildContainer(typeof(T));
            return Resolve<T>();
        }

        /// <summary>
        /// Creates a mock of a given type
        /// </summary>
        /// <typeparam name="T">The type to be mocked</typeparam>
        /// <returns></returns>
        public T Mock<T>() where T : class
        {
            var mock = new Mock<T>();
            mock.SetupAllProperties();
            return mock.Object;
        }

        /// <summary>
        /// Allows a fixture to configure a mock. The configured mock is added to the container when the sut is created.
        /// The consuming test can thus verify the mock by simply resolving it.
        /// </summary>
        /// <typeparam name="T">The type of the mock</typeparam>
        /// <returns></returns>
        protected Mock<T> ConfigureMock<T>() where T : class
        {
            var mock = new Mock<T>();
            mock.SetupAllProperties();
            _configuredMocks.Add(typeof(T), mock);
            return mock;
        }

        /// <summary>
        /// Resolves a dependency from the container. 
        /// If the dependency has not been registered a mock is created (auto-mocking)
        /// Note that the mock cannot be verified against unless it was explicitly configured against the fixture
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Resolve<T>() where T : class
        {
            if (!Container.IsRegistered<T>())
                return Mock<T>();

            using (var scope = Container.BeginLifetimeScope())
            {
                return scope.Resolve<T>();
            }
        }
    }
}
