﻿using NUnit.Framework;
using ScreenToolkit.FluentApi;

namespace ScreenToolkit.Tests.FluentApi
{
    [TestFixture]
    public class FluentApiTests
    {
        [Test]
        public void FluentApi_LaunchApp()
        {
            ScreenAutomation.RegisterServices();

            var appLaunchPath = @"C:\Windows\system32\notepad.exe";

            var sut = new ScreenAutomation();
            sut.LaunchApp().FromFile(appLaunchPath).Type("hello world");
        }
    }
}
