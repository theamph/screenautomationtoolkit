﻿using Moq;
using ScreenToolkit.Core;
using ScreenToolkit.Windows;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ScreenToolkit.Tests.Windows.Fixtures
{
    public class WindowServiceFixture : AutoMockingFixture
    {
        public WindowServiceFixture()
        {
            ConfigureMock<IWindowManager>()
                .Setup(m => m.GetWindow(It.IsAny<IWindow>()))
                .Returns(Mock<Window>());
        }

        public WindowServiceFixture WithMultipleDisplays(int amount = 2)
        {
            var multipleDisplays = new List<Display>();
            for (int i = 1; i <= amount; i++)
            {
                multipleDisplays.Add(Mock<Display>());
            }
            ConfigureMock<IDisplayService>()
                .Setup(d => d.GetDisplays())
                .Returns(multipleDisplays);

            return this;
        }

        public Process GetRealProcessWithWindow()
        {
            return Process.GetProcesses()
                .First(p => !string.IsNullOrWhiteSpace(p.MainWindowTitle));
        }

        public Window CreateRealWindow()
        {
            var process = GetRealProcessWithWindow();
            return CreateRealWindow(process);
        }

        public Window CreateRealWindow(Process process)
        {
            return new Window(process.MainWindowHandle);
        }
    }
}
