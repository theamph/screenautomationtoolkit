﻿using ScreenToolkit.Windows;
using System.Diagnostics;
using System.Linq;

namespace ScreenToolkit.Tests.Windows.Fixtures
{
    public class WindowManagerFixture : AutoMockingFixture
    {
        public Window CreateRealWindow()
        {
            var process = Process.GetProcesses()
                .First(p => !string.IsNullOrWhiteSpace(p.MainWindowTitle));
            return new Window(process.MainWindowHandle);
        }
    }
}
