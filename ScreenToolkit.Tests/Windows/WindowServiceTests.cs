﻿using Moq;
using NUnit.Framework;
using ScreenToolkit.Core;
using ScreenToolkit.Tests.Windows.Fixtures;
using ScreenToolkit.Windows;
using System;
using System.Linq;

namespace ScreenToolkit.Tests.Windows
{
    [TestFixture]
    public class WindowServiceTests
    {
        [Test]
        public void WindowService_Implements_IWindowService()
        {
            var fixture = new WindowServiceFixture();
            var sut = fixture.CreateSut<WindowService>();

            Assert.IsNotNull(sut as IWindowService);
        }

        [Test]
        public void WindowService_CaptureDesktop_ConsidersDisplays()
        {
            var fixture = new WindowServiceFixture();
            var sut = fixture.CreateSut<WindowService>();

            sut.CaptureDesktop();

            fixture.Resolve<IDisplayService>().AsMock().Verify(
                s => s.GetDisplays(), Times.Once);
        }

        [Test]
        public void WindowService_CaptureDesktop_MatchesDisplaySize()
        {
            var fixture = new WindowServiceFixture()
                .WithRealDependencies<WindowServiceFixture>();
            var display = fixture.Resolve<IDisplayService>().GetDisplays().Single();
            var sut = fixture.CreateSut<WindowService>();

            var actual = sut.CaptureDesktop();

            Assert.IsNotNull(actual);
            Assert.AreEqual(display.Bounds.Height, actual.Height);
            Assert.AreEqual(display.Bounds.Width, actual.Width);
        }

        [Test]
        public void WindowService_CaptureDesktop_ThrowsNotSupportedException()
        {
            var fixture = new WindowServiceFixture()
                .WithMultipleDisplays();
            var sut = fixture.CreateSut<WindowService>();
            
            Assert.Throws<NotSupportedException>(
                delegate { sut.CaptureDesktop(); });
        }

        [Test]
        public void WindowService_CaptureWindow_CreatesAValidImage()
        {
            var fixture = new WindowServiceFixture()
                .WithRealDependencies<WindowServiceFixture>();
            var realWindow = fixture.CreateRealWindow();
            var sut = fixture.CreateSut<WindowService>();

            var actual = sut.CaptureWindow(realWindow);

            Assert.IsNotNull(actual);
            Assert.IsFalse(actual.Size.IsEmpty);
        }

        [Test]
        public void WindowService_CaptureWindow_ThrowsArgumentNullException()
        {
            var fixture = new WindowServiceFixture();
            var sut = fixture.CreateSut<WindowService>();

            Assert.Throws<ArgumentNullException>(
                delegate { sut.CaptureWindow(null); });
        }

        [Test]
        public void WindowService_SetForeground_InvokesWindowsApi()
        {
            var fixture = new WindowServiceFixture();
            var sut = fixture.CreateSut<WindowService>();
            var windowMock = fixture.Mock<IWindow>();

            sut.SetForeground(windowMock);

            fixture.Resolve<IWindowsApi>().AsMock().Verify(
                api => api.SetForegroundWindow(It.IsAny<IntPtr>()), Times.Once);
        }

        [Test]
        public void WindowService_SetForeground_ThrowsArgumentNullException()
        {
            var fixture = new WindowServiceFixture();
            var sut = fixture.CreateSut<WindowService>();

            Assert.Throws<ArgumentNullException>(
                delegate { sut.SetForeground(null); });
        }

        [Test]
        public void WindowService_GetWindow_ReturnsWindowForValidProcess()
        {
            var fixture = new WindowServiceFixture()
                .WithRealDependencies<WindowServiceFixture>();
            var sut = fixture.CreateSut<WindowService>();
            var process = fixture.GetRealProcessWithWindow();
            var expected = fixture.CreateRealWindow(process);

            var actual = sut.GetWindow(process);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WindowService_GetWindow_ThrowsNotSupportedException()
        {
            var fixture = new WindowServiceFixture()
                .WithMultipleDisplays();
            var sut = fixture.CreateSut<WindowService>();

            object invalidOwner = "notValid";

            Assert.Throws<NotSupportedException>(
                delegate { sut.GetWindow(invalidOwner); });
        }

        [Test]
        public void WindowService_GetWindow_ThrowsArgumentNullException()
        {
            var fixture = new WindowServiceFixture();
            var sut = fixture.CreateSut<WindowService>();

            Assert.Throws<ArgumentNullException>(
                delegate { sut.GetWindow(null); });
        }
    }
}
