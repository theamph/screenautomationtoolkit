﻿using Moq;
using NUnit.Framework;
using ScreenToolkit.Core;
using ScreenToolkit.Tests.Windows.Fixtures;
using ScreenToolkit.Windows;
using System;

namespace ScreenToolkit.Tests.Windows
{
    public class WinWindowManagerTests
    {
        [Test]
        public void WindowManager_GetWindow_ReturnsWindow()
        {
            var fixture = new WindowManagerFixture();
            var sut = fixture.CreateSut<WindowManager>();
            var expected = fixture.CreateRealWindow();

            var actual = sut.GetWindow(expected);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WindowManager_GetWindow_ReturnsNullWhenNotFound()
        {
            var fixture = new WindowManagerFixture();
            var sut = fixture.CreateSut<WindowManager>();
            var fakeWindow = fixture.Mock<IWindow>();

            var app = sut.GetWindow(fakeWindow);

            Assert.IsNull(app);
        }

        [Test]
        public void WindowManager_GetWindow_ThrowsArgumentNullException()
        {
            var fixture = new WindowManagerFixture();
            var sut = fixture.CreateSut<WindowManager>();

            Assert.Throws<ArgumentNullException>(
                delegate { sut.GetWindow(null as IWindow); });
        }
    }
}
