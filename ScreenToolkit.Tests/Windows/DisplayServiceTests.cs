﻿using NUnit.Framework;
using ScreenToolkit.Core;
using ScreenToolkit.Tests.Windows.Fixtures;
using ScreenToolkit.Windows;
using System.Linq;

namespace ScreenToolkit.Tests.Core.ScreenTests
{
    public class DisplayServiceTests
    {
        [Test]
        public void DisplayService_Implements_IDisplayService()
        {
            var fixture = new DisplayServiceFixture();
            var sut = fixture.CreateSut<DisplayService>();
            Assert.IsNotNull(sut as IDisplayService);
        }
        
        [Test]
        public void DisplayService_GetDisplays_ReturnsListOfDisplays()
        {
            var fixture = new DisplayServiceFixture();
            var sut = fixture.CreateSut<DisplayService>();

            var actual = sut.GetDisplays();

            Assert.IsNotNull(actual);
            Assert.That(actual.Count(), Is.GreaterThan(0));
        }
    }
}
