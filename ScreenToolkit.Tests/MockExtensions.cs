﻿using Moq;

namespace ScreenToolkit.Tests
{
    public static class MockExtensions
    {
        public static Mock<T> AsMock<T>(this T obj) where T : class
        {
            return Mock.Get(obj);
        }
    }
}
