﻿using Autofac;
using Moq;
using ScreenToolkit.Core;
using ScreenToolkit.Windows;

namespace ScreenToolkit.Tests
{
    public class MockModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterMock<IWindowManager>(builder);
            RegisterMock<IWindowsApi>(builder);
            RegisterMock<IDisplayService>(builder);
            RegisterMock<IInputService>(builder);
            RegisterMock<IWindowService>(builder);
        }

        protected void RegisterMock<T>(ContainerBuilder builder) where T : class
        {
            builder.RegisterInstance(new Mock<T>().Object).As<T>();
        }
    }
}
