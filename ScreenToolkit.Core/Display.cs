﻿using System.Drawing;

namespace ScreenToolkit.Core
{
    public class Display
    {
        public string Name { get; set; }
        public Rectangle Bounds { get; set; }
        public Rectangle WorkArea { get; set; }
        public bool IsPrimary { get; set; }
    }
}
