﻿using System.Collections.Generic;

namespace ScreenToolkit.Core
{
    public interface IDisplayService
    {
        IEnumerable<Display> GetDisplays();
    }
}
