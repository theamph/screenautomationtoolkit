﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenToolkit.Core
{
    public interface IWindow
    {
        object Id { get; }
        string FriendlyName { get; }
    }
}
