﻿using System.Drawing;

namespace ScreenToolkit.Core
{
    public interface IWindowService
    {
        Image CaptureDesktop();
        Image CaptureWindow(IWindow window);
        void SetForeground(IWindow window);
        IWindow GetWindow(object owner);
    }
}
