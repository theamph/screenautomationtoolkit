﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenToolkit.Core
{
    public interface IInputService
    {
        void Type(char inputChar, IWindow targetWindow);
        void Type(string inputText, IWindow targetWindow);
    }
}
