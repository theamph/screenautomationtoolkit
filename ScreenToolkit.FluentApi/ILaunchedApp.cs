﻿namespace ScreenToolkit.FluentApi
{
    public interface ILaunchedApp
    {
        void Type(string text);
    }
}