﻿using Autofac;
using ScreenToolkit.Core;
using ScreenToolkit.Windows;

namespace ScreenToolkit.FluentApi
{
    public class ScreenAutomation
    {
        private static IContainer Container { get; set; }

        public static void RegisterServices()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new WindowsModule());
            Container = builder.Build();
        }

        public IAppLauncher LaunchApp()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var windowService = scope.Resolve<IWindowService>();
                var inputService = scope.Resolve<IInputService>();
                return new AppLauncher(windowService, inputService);
            }
        }
    }
}
