﻿using ScreenToolkit.Core;
using System.Diagnostics;

namespace ScreenToolkit.FluentApi
{
    public class AppLauncher : IAppLauncher
    {
        private readonly IWindowService _windowService;
        private readonly IInputService _inputService;

        public AppLauncher(IWindowService windowService, IInputService inputService)
        {
            _windowService = windowService;
            _inputService = inputService;
        }

        public ILaunchedApp FromFile(string filePath)
        {
            var process = Process.Start(filePath);
            return new LaunchedApp(process, _windowService, _inputService);
        }
    }
}
