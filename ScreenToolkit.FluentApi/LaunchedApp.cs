﻿using ScreenToolkit.Core;
using System.Diagnostics;

namespace ScreenToolkit.FluentApi
{
    public class LaunchedApp : ILaunchedApp
    {
        private readonly IWindow _mainWindow;
        private readonly IWindowService _windowService;
        private readonly IInputService _inputService;

        public LaunchedApp(Process process, IWindowService windowService, 
            IInputService inputService)
        {
            _windowService = windowService;
            _inputService = inputService;
            _mainWindow = _windowService.GetWindow(process);
        }

        public void Type(string text)
        {
            _inputService.Type(text, _mainWindow);
        }
    }
}
