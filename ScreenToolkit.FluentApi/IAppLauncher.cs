﻿namespace ScreenToolkit.FluentApi
{
    public interface IAppLauncher
    {
        ILaunchedApp FromFile(string filePath);
    }
}