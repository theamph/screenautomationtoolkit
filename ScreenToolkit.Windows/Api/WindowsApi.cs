﻿using ManagedWinapi;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ScreenToolkit.Windows.Api
{
    public class WindowsApi : IWindowsApi
    {
        public virtual Image CaptureWindow(IntPtr handle)
        {
            // get te hDC of the target window
            IntPtr hdcSrc = User32.GetWindowDC(handle);
            // get the size
            User32.RECT windowRect = new User32.RECT();
            User32.GetWindowRect(handle, ref windowRect);
            int width = windowRect.right - windowRect.left;
            int height = windowRect.bottom - windowRect.top;
            // create a device context we can copy to
            IntPtr hdcDest = Gdi32.CreateCompatibleDC(hdcSrc);
            // create a bitmap we can copy it to,
            // using GetDeviceCaps to get the width/height
            IntPtr hBitmap = Gdi32.CreateCompatibleBitmap(hdcSrc, width, height);
            // select the bitmap object
            IntPtr hOld = Gdi32.SelectObject(hdcDest, hBitmap);
            // bitblt over
            Gdi32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, Gdi32.SRCCOPY);
            // restore selection
            Gdi32.SelectObject(hdcDest, hOld);
            // clean up 
            Gdi32.DeleteDC(hdcDest);
            User32.ReleaseDC(handle, hdcSrc);
            // get a .NET image object for it
            Image img = Image.FromHbitmap(hBitmap);
            // free up the Bitmap object
            Gdi32.DeleteObject(hBitmap);
            return img;
        }

        public IntPtr GetDesktopWindow()
        {
            return User32.GetDesktopWindow();
        }

        public IntPtr FindWindow(string title)
        {
            return User32.FindWindow(null, title);
        }

        public bool SetForegroundWindow(IntPtr handle)
        {
            return User32.SetForegroundWindow(handle);
        }

        public void SetWindowDisplayState(IntPtr handle, WindowDisplayState displayState)
        {
            User32.ShowWindowAsync(handle, (int)displayState);
        }

        public void SetActiveWindow(IntPtr handle)
        {
            User32.SetActiveWindow(handle);
        }

        public void InputCharacter(char character)
        {
            var key = MapCharToKey(character);
            var entry = new KeyboardKey(key);
            entry.PressAndRelease();
        }

        private Keys MapCharToKey(char character)
        {
            if (character.ToString() == " ")
                return Keys.Space;
            return (Keys)Enum.Parse(typeof(Keys),
                character.ToString().ToUpper());
        }
    }
}
