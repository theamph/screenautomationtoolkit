﻿using ManagedWinapi.Windows;
using ScreenToolkit.Core;
using System;

namespace ScreenToolkit.Windows
{
    public class Window : IWindow
    {
        private SystemWindow _systemWindow;

        public object Id
        {
            get { return _systemWindow.HWnd; }
        }

        public string FriendlyName
        {
            get {
                return string.Format("{0} ({1})", 
                    _systemWindow.Title, 
                    _systemWindow.Process.ProcessName);
            }
        }

        public IntPtr Handle
        {
            get { return _systemWindow.HWnd; }
        }

        public Window()
        {
            _systemWindow = new SystemWindow(IntPtr.Zero);
        }

        public Window(IntPtr handle)
        {
            _systemWindow = new SystemWindow(handle);
        }

        public override bool Equals(object obj)
        {
            var other = obj as Window;
            if (other == null)
                return false;
            return _systemWindow.Equals(
                other._systemWindow);
        }

        public override int GetHashCode()
        {
            return _systemWindow.GetHashCode();
        }
    }
}
