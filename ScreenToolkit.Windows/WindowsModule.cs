﻿using Autofac;
using ScreenToolkit.Core;
using ScreenToolkit.Windows;
using ScreenToolkit.Windows.Api;

namespace ScreenToolkit.Windows
{
    public class WindowsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WindowManager>().As<IWindowManager>();
            builder.RegisterType<WindowsApi>().As<IWindowsApi>();
            builder.RegisterType<DisplayService>().As<IDisplayService>();
            builder.RegisterType<InputService>().As<IInputService>();
            builder.RegisterType<WindowService>().As<IWindowService>();
        }
    }
}
