﻿using ScreenToolkit.Core;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace ScreenToolkit.Windows
{
    public class WindowService : IWindowService
    {
        private readonly IWindowManager _windowManager;
        private readonly IDisplayService _displayService;
        private readonly IWindowsApi _windowsApi;

        public WindowService(IWindowManager windowManager, 
            IDisplayService displayManager, IWindowsApi windowsApi)
        {
            _windowManager = windowManager;
            _displayService = displayManager;
            _windowsApi = windowsApi;
        }

        public virtual Image CaptureDesktop()
        {
            var displays = _displayService.GetDisplays();
            if (displays.Count() > 1)
                throw new NotSupportedException("Multiple displays are not supported");
            var desktopHandle = _windowsApi.GetDesktopWindow();
            return _windowsApi.CaptureWindow(desktopHandle);
        }

        public virtual Image CaptureWindow(IWindow window)
        {
            if (window == null)
                throw new ArgumentNullException("window");
            var appWindow = _windowManager.GetWindow(window);
            return _windowsApi.CaptureWindow(appWindow.Handle);
        }

        public virtual void SetForeground(IWindow window)
        {
            if (window == null)
                throw new ArgumentNullException("window");
            var appWindow = _windowManager.GetWindow(window);
            _windowsApi.SetWindowDisplayState(appWindow.Handle, WindowDisplayState.Restore);
            _windowsApi.SetForegroundWindow(appWindow.Handle);
        }

        public IWindow GetWindow(object owner)
        {
            if (owner == null)
                throw new ArgumentNullException("owner");
            if (owner is Process)
                return _windowManager.GetWindow(owner as Process);

            var notSupported = string.Format("Owner of type {0} is not supported", 
                owner.GetType());
            throw new NotSupportedException(notSupported);
        }

        protected virtual void Maximize(IWindow window)
        {
            if (window == null)
                throw new ArgumentNullException("window");
            var appWindow = _windowManager.GetWindow(window);
            _windowsApi.SetWindowDisplayState(appWindow.Handle, WindowDisplayState.Maximized);
        }

        protected virtual void Minimize(IWindow window)
        {
            if (window == null)
                throw new ArgumentNullException("window");
            var appWindow = _windowManager.GetWindow(window);
            _windowsApi.SetWindowDisplayState(appWindow.Handle, WindowDisplayState.Minimized);
        }
    }
}
