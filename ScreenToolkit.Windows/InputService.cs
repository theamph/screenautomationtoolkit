﻿using ScreenToolkit.Core;
using System;
using System.Linq;

namespace ScreenToolkit.Windows
{
    public class InputService : IInputService
    {
        private readonly IWindowManager _windowManager;
        private readonly IWindowsApi _windowsApi;

        public InputService(IWindowManager windowManager, 
            IWindowsApi windowsApi)
        {
            _windowManager = windowManager;
            _windowsApi = windowsApi;
        }

        public void Type(char inputChar, IWindow targetWindow)
        {
            if (targetWindow == null)
                throw new ArgumentNullException("targetWindow");
            var appWindow = _windowManager.GetWindow(targetWindow);
            Type(inputChar, appWindow);
        }

        public void Type(string inputText, IWindow targetWindow)
        {
            if (inputText == null)
                throw new ArgumentNullException("inputText");
            if (targetWindow == null)
                throw new ArgumentNullException("targetWindow");

            var appWindow = _windowManager.GetWindow(targetWindow);
            inputText.ToCharArray().ToList()
                .ForEach(c => Type(c, appWindow));
        }

        private void Type(char inputChar, Window targetWindow)
        {
            // Todo: Is there a bulletproof way to focus input stream on the target window?
            // The focus seems to get lost.. (is it taken over by this thread? check with Spy++)
            _windowsApi.SetActiveWindow(targetWindow.Handle);
            _windowsApi.InputCharacter(inputChar);
        }
    }
}
