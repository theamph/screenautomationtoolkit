﻿using ScreenToolkit.Core;
using System.Diagnostics;

namespace ScreenToolkit.Windows
{
    public interface IWindowManager
    {
        Window GetWindow(IWindow reference);
        Window GetWindow(Process process);
    }
}
