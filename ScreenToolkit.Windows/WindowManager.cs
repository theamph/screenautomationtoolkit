﻿using ScreenToolkit.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ScreenToolkit.Windows
{
    public class WindowManager : IWindowManager
    {
        private readonly IWindowsApi _windowsApi;

        public WindowManager(IWindowsApi windowsApi)
        {
            _windowsApi = windowsApi;
        }

        public virtual Window GetWindow(IWindow reference)
        {
            if (reference == null)
                throw new ArgumentNullException("reference");

            try
            {
                return new Window((IntPtr)reference.Id);
            }
            catch
            {
                return null;
            }
        }

        public virtual Window GetWindow(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            return new Window(process.MainWindowHandle);
        }
    }
}
