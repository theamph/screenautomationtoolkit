﻿using ScreenToolkit.Core;
using System.Collections.Generic;

namespace ScreenToolkit.Windows
{
    public class DisplayService : IDisplayService
    {
        public virtual IEnumerable<Display> GetDisplays()
        {
            foreach (var screen in System.Windows.Forms.Screen.AllScreens)
            {
                yield return new Display()
                {
                    Name = screen.DeviceName,
                    Bounds = screen.Bounds,
                    WorkArea = screen.WorkingArea,
                    IsPrimary = screen.Primary
                };
            }
        }
    }
}
