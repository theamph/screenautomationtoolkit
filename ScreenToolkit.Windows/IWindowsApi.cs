﻿using System;
using System.Drawing;

namespace ScreenToolkit.Windows
{
    public interface IWindowsApi
    {
        Image CaptureWindow(IntPtr handle);
        IntPtr GetDesktopWindow();
        IntPtr FindWindow(string title);
        bool SetForegroundWindow(IntPtr handle);
        void SetActiveWindow(IntPtr handle);
        void SetWindowDisplayState(IntPtr handle, WindowDisplayState displayState);
        void InputCharacter(char character);
    }
}
